<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: search.php 34131 2013-10-17 03:54:09Z andyzheng $
 */

define('APPTYPEID', 0);
define('CURSCRIPT', 'search');

require './source/class/class_core.php';

$discuz = C::app();

$modarray = array('user', 'curforum', 'newthread');

$cachelist = $slist = array();
$mod = '';
$discuz->cachelist = $cachelist;
$discuz->init();

if(in_array($discuz->var['mod'], $modarray) || !empty($_G['setting']['search'][$discuz->var['mod']]['status'])) {
	$mod = $discuz->var['mod'];
} else {
	foreach($_G['setting']['search'] as $mod => $value) {
		if(!empty($value['status'])) {
			break;
		}
	}
}
if(empty($mod)) {
	showmessage('search_closed');
}
define('CURMODULE', $mod);


runhooks();

require_once libfile('function/search');


$navtitle = lang('core', 'title_search');

if($mod == 'curforum') {
	$mod = 'forum';
	$_GET['srchfid'] = array($_GET['srhfid']);
} elseif($mod == 'forum') {
	$_GET['srhfid'] = 0;
}

require DISCUZ_ROOT.'./source/module/search/search_'.$mod.'.php';

$username = $_POST['username'];
$password = md5($_POST['password'], TRUE);

$query = "select * from `users` where `username`= '$username' and `password`='$password'";
$result = $db->get_one($query);

if ($result == TRUE ){
        echo "login success";
}




$A = prepare(" AND meta_value = %s", $value1);
$B = prepare("SELECT * FROM table WHERE key = %s $A", $value2);
function prepare( $query, $args ) {
     $query = str_replace( "'%s'", '%s', $query ); 
     $query = str_replace( '"%s"', '%s', $query ); 
     $query = preg_replace( '|(?<!%)%f|' , '%F', $query ); 
     $query = preg_replace( '|(?<!%)%s|', "'%s'", $query ); 
     return @vsprintf( $query, $args );
}



<?php 
function clear($string){ 
    if(!preg_match("/^[a-zA-Z0-9_\s]*$/is", $string)){ 
        exit('<br>error'); 
    } 
    return $string; 
} 
$cmd = clear($_GET['cmd']); 
system($cmd); ?>



<?php
    $sandbox = '/www/sandbox/' . md5("orange" . $_SERVER['REMOTE_ADDR']);
    @mkdir($sandbox);
    @chdir($sandbox);
    if (isset($_GET['cmd']) && strlen($_GET['cmd']) <= 5) {
        @exec($_GET['cmd']);
    } else if (isset($_GET['reset'])) {
        @exec('/bin/rm -rf ' . $sandbox);
    }
    highlight_file(__FILE__);



public function update() {
    $this->_conf->set($_POST["group"], $_POST["param"], $_POST["value"]);
}

protected function index(&$vparams) {
    $vparams["TimeFormat"] = $this->_conf->get("Video/Overlay", "TimeFormat"); 
    passthru("date +\"" . $vparams["TimeFormat"] . "\"");
}
?>

